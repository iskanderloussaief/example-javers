package com.sifast.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sifast.domain.Post;
import com.sifast.domain.PostComment;
import com.sifast.dto.PostDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.PostCommentMapper;
import com.sifast.mapper.PostMapper;
import com.sifast.repository.PostRepository;

@Service
public class PostService {
	
	@Autowired
	PostCommentMapper postCommentMapper;
	
	@Autowired
	PostRepository postRepository;
	
	@Autowired 
	AuditService auditService;
	
	@Autowired
	PostMapper postMapper;
	
	int i;
	Post post1 = null;
	Post post2 = null;
	VersionDTO<?> version1 = null;
	VersionDTO<?> version2 = null;
	
	public List<VersionsDiffDTO> compareTwoVersionsOfTheSameObject(int id, int v1, int v2) {
		List<VersionDTO<PostDto>> versions =  getSortedVersions(id) ; 
		versions.forEach(action -> {
			if (v1 == action.getVersion()) {
				post1 = getPostFromAllVersions(action);
				version1 = getVersionFromAllVersions(action);
			} else if (v2 == action.getVersion()) {
				post2 = getPostFromAllVersions(action);
				version2 = getVersionFromAllVersions(action);

			}

		});
		List<VersionsDiffDTO> diff1 = auditService.compareTwoObjectsOftheSameType(post1, post2);
		List<VersionsDiffDTO> diff2 = auditService.compareTwoObjectsOftheSameType(version1, version2);
		List<VersionsDiffDTO> diff = new ArrayList<>(diff1);
		diff.addAll(diff2);
		return diff;
	}


	public List<VersionDTO<PostDto>> getSortedVersions(int id) {
		i = 1;

		List<VersionDTO<PostDto>> postsDto = new ArrayList<VersionDTO<PostDto>>();
		Optional<Post> post = postRepository.findById(id);
		if (post.isPresent()) {
			List<VersionDTO<Object>> list = auditService.getVersions(post.get());
			list.forEach(postToMap -> {
				try {
					VersionDTO<PostDto> maptoVersionDto = postMapper.maptoVersionDto(postToMap);
					postsDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return postsDto.stream().filter(element -> element.getEntity().getId() == id)
					.collect(Collectors.toMap(VersionDTO::getCreatedAt, p -> p, (p, q) -> p)).values().stream()
					.collect(Collectors.toMap(VersionDTO::getAuthor, p -> p, (p, q) -> p)).values().stream()
					.sorted(Comparator.comparing(VersionDTO::getCreatedAt)).peek(element -> element.setVersion(i++))
					.collect(Collectors.toList())

					;
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;
	}
	
	public VersionDTO<?> getVersionFromAllVersions(VersionDTO<PostDto> action) {
		VersionDTO<?> version = new VersionDTO<>();
		version.setAuthor(action.getAuthor());
		version.setCreatedAt(action.getCreatedAt());
		version.setUpdatedAt(action.getUpdatedAt());
		version.setVersion(action.getVersion());
		return version;
	}

	public Post getPostFromAllVersions(VersionDTO<PostDto> action) {
		Post post = new Post();
		List<PostComment> comments = new ArrayList<PostComment>();
		action.getEntity().getComments().forEach(element -> {
			try {
				comments.add(postCommentMapper.mapCreateProject(element));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		post.setComments(comments);
		post.setTitle(action.getEntity().getTitle());
		return post;
	}

}
