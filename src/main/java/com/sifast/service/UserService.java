package com.sifast.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sifast.domain.Profile;
import com.sifast.domain.User;
import com.sifast.dto.UserDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.UserMapper;
import com.sifast.repository.ProfileRepository;
import com.sifast.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	UserMapper userMapper;

	@Autowired
	ProfileRepository profileRepository;

	int i;
	User user1 = null;
	User user2 = null;
	VersionDTO<?> version1 = null;
	VersionDTO<?> version2 = null;

	public List<VersionsDiffDTO> compareTwoVersionsOfTheSameObject(int id, int v1, int v2) {
		List<VersionDTO<UserDto>> versions = getSortedVersions(id);
		versions.forEach(action -> {
			if (v1 == action.getVersion()) {
				user1 = getUserFromAllVersions(action);
				version1 = getVersionFromAllVersions(action);
			} else if (v2 == action.getVersion()) {
				user2 = getUserFromAllVersions(action);
				version2 = getVersionFromAllVersions(action);

			}

		});
		List<VersionsDiffDTO> diff1 = auditService.compareTwoObjectsOftheSameType(user1, user2);
		List<VersionsDiffDTO> diff2 = auditService.compareTwoObjectsOftheSameType(version1, version2);
		List<VersionsDiffDTO> diff = new ArrayList<>(diff1);
		diff.addAll(diff2);
		return diff;
	}

	public List<VersionDTO<UserDto>> getSortedVersions(int id) {
		i = 1;

		List<VersionDTO<UserDto>> usersDto = new ArrayList<VersionDTO<UserDto>>();
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			List<VersionDTO<Object>> list = auditService.getVersions(user.get());
			list.forEach(userToMap -> {
				try {
					VersionDTO<UserDto> maptoVersionDto = userMapper.maptoVersionDto(userToMap);
					usersDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return usersDto.stream().filter(element -> element.getEntity().getId() == id)
					.collect(Collectors.toMap(VersionDTO::getCreatedAt, p -> p, (p, q) -> p)).values().stream()
					.collect(Collectors.toMap(VersionDTO::getAuthor, p -> p, (p, q) -> p)).values().stream()
					.sorted(Comparator.comparing(VersionDTO::getCreatedAt)).peek(element -> element.setVersion(i++))
					.collect(Collectors.toList())

			;
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;
	}

	public VersionDTO<?> getVersionFromAllVersions(VersionDTO<UserDto> action) {
		VersionDTO<?> version = new VersionDTO<>();
		version.setAuthor(action.getAuthor());
		version.setCreatedAt(action.getCreatedAt());
		version.setUpdatedAt(action.getUpdatedAt());
		version.setVersion(action.getVersion());
		return version;
	}

	public User getUserFromAllVersions(VersionDTO<UserDto> action) {
		User user = new User();
		if (action.getEntity() != null) {
			user.setEmail(action.getEntity().getEmail());
			user.setPassword(action.getEntity().getPassword());
			user.setUsername(user.getUsername());
			int profileId = action.getEntity().getProfile();
			Profile profile = profileRepository.getOne(profileId);
			user.setProfile(profile);
		}
		return user;
	}

}
