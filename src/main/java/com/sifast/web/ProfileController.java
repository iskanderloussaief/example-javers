
package com.sifast.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sifast.domain.Profile;
import com.sifast.dto.ProfileDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.PostMapper;
import com.sifast.mapper.ProfileMapper;
import com.sifast.repository.ProfileRepository;
import com.sifast.service.AuditService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/v1/profile")
@Api(value = "Profile-api", tags = "Profile-api")
@CrossOrigin("*")
public class ProfileController {

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	ProfileMapper profileMapper;

	@Autowired
	PostMapper postMapper;

	@PostMapping("")
	ResponseEntity<Profile> create(@RequestBody ProfileDto profile, Principal principal) {
		Profile profileToSave = profileMapper.mapProfileDtoToProfile(profile);
		profileRepository.save(profileToSave);
		auditService.commit("profileCreateProfile", profileToSave);
		return new ResponseEntity<>(profileToSave, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	ResponseEntity<?> delete(@PathVariable int id) {
		return profileRepository.findById(id).map(p -> {
			profileRepository.delete(p);
			auditService.commitDelete("profileDeleteProfile", p);
			return new ResponseEntity<>(HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("")
	List<ProfileDto> list() {

		return profileRepository.findAll().stream()
				.map(profile -> profileMapper.mapProfileToProfileDto(profile))
				.collect(Collectors.toList());
	}

	@PutMapping("/{id}")
	ResponseEntity<Profile> update(@PathVariable int id, @RequestBody ProfileDto profile, Principal principal) {
		return profileRepository.findById(id).map(p -> {
			p.setId(profile.getId());
			p.setDescription(profile.getDescription());
			p.setStatus(profile.getStatus());
			profileRepository.save(p);
			auditService.commit("profileUpdateProfile", p);
			return new ResponseEntity<>(p, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	int i = 0;

	@GetMapping("{id}/versions")
	ResponseEntity<List<VersionDTO<ProfileDto>>> getVersions(@PathVariable int id) throws Exception {
		List<VersionDTO<ProfileDto>> profilesDto = new ArrayList<VersionDTO<ProfileDto>>();
		Optional<Profile> profile = profileRepository.findById(id);
		if (profile.isPresent()) {

			List<VersionDTO<Object>> list = auditService.getVersions(profile.get());
			list.forEach(profileToMap -> {
				try {
					VersionDTO<ProfileDto> maptoVersionDto = profileMapper.maptoVersionDto(profileToMap);
					profilesDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return new ResponseEntity<>(profilesDto.stream().filter(element -> element.getEntity().getId() == id)
					.peek(element -> element.setVersion(i++)).collect(Collectors.toList()), HttpStatus.OK);
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;

	}

	@GetMapping("/versions/without/{id}")
	ResponseEntity<List<VersionDTO<ProfileDto>>> getVersionsWithoutRelationships(@PathVariable int id) throws Exception {
		List<VersionDTO<ProfileDto>> profilesDto = new ArrayList<VersionDTO<ProfileDto>>();
		Optional<Profile> profile = profileRepository.findById(id);
		if (profile.isPresent()) {

			List<VersionDTO<Object>> list = auditService.getVersionsWithoutRelationships(profile.get(), id);
			list.forEach(profileToMap -> {
				try {
					VersionDTO<ProfileDto> maptoVersionDto = profileMapper.maptoVersionDto(profileToMap);
					profilesDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return new ResponseEntity<>(profilesDto, HttpStatus.OK);
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;

	}

	@GetMapping("versions")
	ResponseEntity<List<VersionDTO<ProfileDto>>> getVersions() throws Exception {
		List<VersionDTO<ProfileDto>> profilesDto = new ArrayList<VersionDTO<ProfileDto>>();

		List<VersionDTO<Object>> list = auditService.getVersions();
		list.forEach(profileToMap -> {
			try {
				VersionDTO<ProfileDto> maptoVersionDto = profileMapper.maptoVersionDto(profileToMap);
				profilesDto.add(maptoVersionDto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return new ResponseEntity<>(profilesDto, HttpStatus.OK);

	}

	@GetMapping("{id}/versions/diff")
	ResponseEntity<List<VersionsDiffDTO>> getDiff(@PathVariable int id, @RequestParam int left,
			@RequestParam int right) {

		return profileRepository.findById(id).map(p -> {
			List<VersionsDiffDTO> diff = auditService.compare(Profile.class, id, left, right);
			return new ResponseEntity<>(diff, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@PutMapping("/{id}/versions")
	ResponseEntity<Profile> changeVersion(@PathVariable int id, @RequestHeader Integer version) {
		return profileRepository.findById(id).map(cs -> {
			Profile c = auditService.getVersion(Profile.class, id, version);
			profileRepository.save(c);
			return new ResponseEntity<>(c, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

}
