
package com.sifast.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sifast.domain.PostComment;
import com.sifast.dto.PostCommentDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.PostCommentMapper;
import com.sifast.repository.PostCommentRepository;
import com.sifast.repository.PostRepository;
import com.sifast.service.AuditService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/v1/postComments")
@Api(value = "Post-Comment-api", tags = "Post-Comment-api")
@CrossOrigin("*")
public class PostCommentController {

    @Autowired
    PostCommentRepository postCommentRepository;

    @Autowired
    PostRepository postRepository;


    @Autowired
    AuditService auditService;

    @Autowired
    PostCommentMapper postCommentMapper;


    private Object httpResponseBody;

    private HttpStatus httpStatus;

    @PostMapping("")
    ResponseEntity<?> create(@RequestBody PostCommentDto post, Principal principal) throws Exception {
        PostComment postToSave = postCommentMapper.mapCreateProject(post);
        postCommentRepository.save(postToSave);
        auditService.commit("userCreatePostComment", postToSave);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    ResponseEntity<?> delete(@PathVariable int id) {
        return postCommentRepository.findById(id).map(p -> {
            postCommentRepository.delete(p);
            auditService.commitDelete("userDelete", p);
            return new ResponseEntity<>(HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @GetMapping("")
    ResponseEntity<?> list() {
        List<PostComment> postcomments = postCommentRepository.findAll();
        httpStatus = HttpStatus.OK;
        httpResponseBody = !postcomments.isEmpty() ?
                postcomments.stream().map(postComment ->
                        {
							try {
								return postCommentMapper.maptoDto(postComment);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return null;
						}
                ).collect(Collectors.toList()) : Collections.emptyList();

        return new ResponseEntity<>(httpResponseBody, httpStatus);
    }

    @PutMapping("/{id}")
    ResponseEntity<?> update(@PathVariable int id, @RequestBody PostCommentDto post, Principal principal) {
        return postCommentRepository.findById(id).map(p -> {
            p.setPost(postRepository.findById(post.getPostId()).get());
            p.setReview(post.getReview());
            postCommentRepository.save(p);
            auditService.commit("userUpdate", p);
            return new ResponseEntity<>(HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("{id}/versions")
    ResponseEntity<List<VersionDTO<PostCommentDto>>> getVersions(@PathVariable int id) throws Exception {
        List<VersionDTO<PostCommentDto>> postsCommentDto = new ArrayList<VersionDTO<PostCommentDto>>();
        Optional<PostComment> postcomment = postCommentRepository.findById(id);
        if (postcomment.isPresent()) {
            List<VersionDTO<Object>> list = auditService.getVersions(postcomment.get());
            list.forEach(postCommmentToMap -> {
                try {
                    VersionDTO<PostCommentDto> maptoVersionDto = postCommentMapper.maptoVersionDto(postCommmentToMap);
                    postsCommentDto.add(maptoVersionDto);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            });
            return new ResponseEntity<>(postsCommentDto, HttpStatus.OK);
        } else {
            new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return null;


    }

    @GetMapping("{id}/versions/diff")
    ResponseEntity<List<VersionsDiffDTO>> getDiff(@PathVariable int id, @RequestParam int left, @RequestParam int right) {

        return postCommentRepository.findById(id).map(p -> {
            List<VersionsDiffDTO> diff = auditService.compare(PostComment.class, id, left, right);
            return new ResponseEntity<>(diff, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}/versions")
    ResponseEntity<PostComment> changeVersion(@PathVariable int id, @RequestHeader Integer version) {

        return postCommentRepository.findById(id).map(cs -> {
            PostComment c = auditService.getVersion(PostComment.class, id, version);
            postCommentRepository.save(c);
            return new ResponseEntity<>(c, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
