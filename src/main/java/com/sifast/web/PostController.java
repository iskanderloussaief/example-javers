
package com.sifast.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sifast.domain.Post;
import com.sifast.dto.PostDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.PostCommentMapper;
import com.sifast.mapper.PostMapper;
import com.sifast.repository.PostRepository;
import com.sifast.service.AuditService;
import com.sifast.service.PostService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/v1/post")
@Api(value = "Post-api", tags = "Post-api")
@CrossOrigin("*")
public class PostController {

	@Autowired
	PostService postService;

	@Autowired
	PostRepository postRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	PostMapper postMapper;

	@Autowired
	PostCommentMapper postCommentMapper;

	@PostMapping("")
	ResponseEntity<Post> create(@RequestBody PostDto post, Principal principal) {
		Post postToSave = postMapper.mapCreateProject(post);
		postRepository.save(postToSave);
		auditService.commit("userCreatePost", postToSave);
		return new ResponseEntity<>(postToSave, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	ResponseEntity<?> delete(@PathVariable int id) {
		return postRepository.findById(id).map(p -> {
			postRepository.delete(p);
			auditService.commitDelete("userDeletePost", p);
			return new ResponseEntity<>(HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("/changes")
	ResponseEntity<?> find() {

		return new ResponseEntity<>(auditService.getVersionsFinders(), HttpStatus.OK);

	}

	@GetMapping("")
	List<PostDto> list() {

		return postRepository.findAll().stream().map(post -> postMapper.mapPostToPostDto(post))
				.collect(Collectors.toList());
	}

	@PutMapping("/{id}")
	ResponseEntity<Post> update(@PathVariable int id, @RequestBody PostDto post, Principal principal) {
		return postRepository.findById(id).map(p -> {
			p.setTitle(post.getTitle());
			postRepository.save(p);
			auditService.commit("userUpdatePost", p);
			return new ResponseEntity<>(p, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("/versions/{id}")
	ResponseEntity<List<?>> getVersions(@PathVariable int id) throws Exception {
		return new ResponseEntity<>(postService.getSortedVersions(id), HttpStatus.OK);

	}

	@GetMapping("/versions/without/{id}")
	ResponseEntity<List<VersionDTO<PostDto>>> getVersionsWithoutRelationships(@PathVariable int id) throws Exception {
		List<VersionDTO<PostDto>> postsDto = new ArrayList<VersionDTO<PostDto>>();
		Optional<Post> post = postRepository.findById(id);
		if (post.isPresent()) {

			List<VersionDTO<Object>> list = auditService.getVersionsWithoutRelationships(post.get(), id);
			list.forEach(postToMap -> {
				try {
					VersionDTO<PostDto> maptoVersionDto = postMapper.maptoVersionDto(postToMap);
					postsDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return new ResponseEntity<>(postsDto, HttpStatus.OK);
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;

	}

	@GetMapping("versions")
	ResponseEntity<List<VersionDTO<PostDto>>> getVersions() throws Exception {
		List<VersionDTO<PostDto>> postsDto = new ArrayList<VersionDTO<PostDto>>();

		List<VersionDTO<Object>> list = auditService.getVersions();
		list.forEach(postToMap -> {
			try {
				VersionDTO<PostDto> maptoVersionDto = postMapper.maptoVersionDto(postToMap);
				postsDto.add(maptoVersionDto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return new ResponseEntity<>(postsDto, HttpStatus.OK);

	}

	@GetMapping("{id}/versions/diff")
	ResponseEntity<List<VersionsDiffDTO>> getDiff(@PathVariable int id, @RequestParam int left,
			@RequestParam int right) {

		return postRepository.findById(id).map(p -> {
			List<VersionsDiffDTO> diff = auditService.compare(Post.class, id, left, right);
			return new ResponseEntity<>(diff, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@PutMapping("/{id}/versions")
	ResponseEntity<Post> changeVersion(@PathVariable int id, @RequestHeader Integer version) {
		return postRepository.findById(id).map(cs -> {
			Post c = auditService.getVersion(Post.class, id, version);
			postRepository.save(c);
			return new ResponseEntity<>(c, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	
	@GetMapping("/versions/compare/{id}/{v1}/{v2}")
	ResponseEntity<List<VersionsDiffDTO>> compareTwoObjects(@PathVariable int id, @PathVariable int v1,
			@PathVariable int v2) throws Exception {
		List<VersionsDiffDTO> diff = postService.compareTwoVersionsOfTheSameObject(id, v1, v2);
		return new ResponseEntity<>(diff, HttpStatus.OK);

	}

}
