
package com.sifast.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sifast.domain.Profile;
import com.sifast.domain.User;
import com.sifast.dto.UserDto;
import com.sifast.dto.VersionDTO;
import com.sifast.dto.VersionsDiffDTO;
import com.sifast.mapper.PostMapper;
import com.sifast.mapper.UserMapper;
import com.sifast.repository.ProfileRepository;
import com.sifast.repository.UserRepository;
import com.sifast.service.AuditService;
import com.sifast.service.UserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/v1/user")
@Api(value = "User-api", tags = "User-api")
@CrossOrigin("*")
public class UserController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	AuditService auditService;

	@Autowired
	UserMapper userMapper;

	@Autowired
	PostMapper postMapper;

	@Autowired
	UserService userService;

	@PostMapping("")
	ResponseEntity<User> create(@RequestBody UserDto user, Principal principal) {
		User userToSave = userMapper.mapUserDtoToUser(user);
		userRepository.save(userToSave);
		auditService.commit("userCreateUser", userToSave);
		return new ResponseEntity<>(userToSave, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	ResponseEntity<?> delete(@PathVariable int id) {
		return userRepository.findById(id).map(p -> {
			userRepository.delete(p);
			auditService.commitDelete("userDeleteUser", p);
			return new ResponseEntity<>(HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("")
	List<UserDto> list() {

		return userRepository.findAll().stream().map(user -> userMapper.mapUserDtoToUser(user))
				.collect(Collectors.toList());
	}

	@PutMapping("/{id}")
	ResponseEntity<User> update(@PathVariable int id, @RequestBody UserDto user, Principal principal) {
		return userRepository.findById(id).map(p -> {
			p.setId(user.getId());
			p.setUsername(user.getUsername());
			p.setPassword(user.getPassword());
			p.setEmail(user.getEmail());
			Optional<Profile> profile = profileRepository.findById(user.getProfile());
			if (profile.isPresent()) {
				p.setProfile(profile.get());
			}
			userRepository.save(p);
			auditService.commit("userUpdateUser", p);
			return new ResponseEntity<>(p, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("/versions/{id}")
	ResponseEntity<List<?>> getVersions(@PathVariable int id) throws Exception {

		return new ResponseEntity<>(userService.getSortedVersions(id), HttpStatus.OK);
	}

	@GetMapping("/versions/without/{id}")
	ResponseEntity<List<VersionDTO<UserDto>>> getVersionsWithoutRelationships(@PathVariable int id) throws Exception {
		List<VersionDTO<UserDto>> usersDto = new ArrayList<VersionDTO<UserDto>>();
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {

			List<VersionDTO<Object>> list = auditService.getVersionsWithoutRelationships(user.get(), id);
			list.forEach(userToMap -> {
				try {
					VersionDTO<UserDto> maptoVersionDto = userMapper.maptoVersionDto(userToMap);
					usersDto.add(maptoVersionDto);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return new ResponseEntity<>(usersDto, HttpStatus.OK);
		} else {
			new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return null;

	}

	@GetMapping("versions")
	ResponseEntity<List<VersionDTO<UserDto>>> getVersions() throws Exception {
		List<VersionDTO<UserDto>> usersDto = new ArrayList<VersionDTO<UserDto>>();

		List<VersionDTO<Object>> list = auditService.getVersions();
		list.forEach(userToMap -> {
			try {
				VersionDTO<UserDto> maptoVersionDto = userMapper.maptoVersionDto(userToMap);
				usersDto.add(maptoVersionDto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return new ResponseEntity<>(usersDto, HttpStatus.OK);

	}

	@GetMapping("{id}/versions/diff")
	ResponseEntity<List<VersionsDiffDTO>> getDiff(@PathVariable int id, @RequestParam int left,
			@RequestParam int right) {

		return userRepository.findById(id).map(p -> {
			List<VersionsDiffDTO> diff = auditService.compare(User.class, id, left, right);
			return new ResponseEntity<>(diff, HttpStatus.OK);
		}).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("/versions/compare/{id}/{v1}/{v2}")
	ResponseEntity<List<VersionsDiffDTO>> compareTwoObjects(@PathVariable int id, @PathVariable int v1,
			@PathVariable int v2) throws Exception {
		List<VersionsDiffDTO> diff = userService.compareTwoVersionsOfTheSameObject(id, v1, v2);
		return new ResponseEntity<>(diff, HttpStatus.OK);

	}

}
