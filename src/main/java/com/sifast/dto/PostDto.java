package com.sifast.dto;

import java.util.List;

public class PostDto{

	private int id;
	private String title;
	private List<PostCommentDto> comments;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<PostCommentDto> getComments() {
		return comments;
	}

	public void setComments(List<PostCommentDto> comments) {
		this.comments = comments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}





}
