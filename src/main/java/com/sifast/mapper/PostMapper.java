package com.sifast.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sifast.domain.Post;
import com.sifast.domain.PostComment;
import com.sifast.dto.PostCommentDto;
import com.sifast.dto.PostDto;
import com.sifast.dto.VersionDTO;

@Component
public class PostMapper {

	@Autowired
	private ConfiguredModelMapper modelMapper;

	@Autowired
	private PostCommentMapper postCommentMapper;

	public Post mapCreateProject(PostDto postDto) {
		Post mappedProject = modelMapper.map(postDto, Post.class);
		return mappedProject;
	}

	public VersionDTO<PostDto> maptoVersionDto(VersionDTO<Object> post) throws Exception {
		VersionDTO<PostDto> dto = new VersionDTO<PostDto>();
		PostDto postDto = new PostDto();
		if (post != null) {
			mapVersionWithoutEntity(post, dto);
			if (post.getEntity() instanceof Post) {
				Post entity = (Post) post.getEntity();
				postDto = modelMapper.map(entity, PostDto.class);
				List<PostCommentDto> peek = postDto.getComments().stream().peek(element->element.setPostId(entity.getId())).collect(Collectors.toList());
				postDto.setComments(peek);
			}
			postDto = trackChildren(post, postDto);
		}
		dto.setEntity(postDto);
		return dto;
	}

	private PostDto trackChildren(VersionDTO<Object> post, PostDto postDto) {
		if (post.getEntity() instanceof PostComment) {
			Post entity = ((PostComment) post.getEntity()).getPost();
			postDto = modelMapper.map(entity, PostDto.class);
			List<PostCommentDto> peek = postDto.getComments().stream().peek(element->element.setPostId(entity.getId())).collect(Collectors.toList());
			postDto.setComments(peek);
		}
		return postDto;
	}

	private void mapVersionWithoutEntity(VersionDTO<Object> post, VersionDTO<PostDto> dto) {
		dto.setAuthor(post.getAuthor());
		dto.setCreatedAt(post.getCreatedAt());
		dto.setCurrentVersion(post.isCurrentVersion());
		dto.setVersion(post.getVersion());
		dto.setUpdatedAt(post.getUpdatedAt());
	}

	public PostDto mapPostToPostDto(Post post) {
		PostDto postDto = new PostDto();
		List<PostCommentDto> comments = new ArrayList<PostCommentDto>();
		postDto.setTitle(post.getTitle());
		postDto.setId(post.getId());
		post.getComments().forEach(comment -> {
			try {
				comments.add(postCommentMapper.maptoDto(comment));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		postDto.setComments(comments);
		return postDto;
	}
}
