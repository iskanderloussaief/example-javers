package com.sifast.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sifast.domain.Profile;
import com.sifast.dto.ProfileDto;
import com.sifast.dto.VersionDTO;
@Component
public class ProfileMapper {
	
	@Autowired
    private ConfiguredModelMapper modelMapper;
	
	
	public Profile mapProfileDtoToProfile(ProfileDto profileDto) {
		Profile mappedProfile = modelMapper.map(profileDto, Profile.class);
        return mappedProfile;
    }
	
	public ProfileDto mapProfileToProfileDto(Profile profile) {
		ProfileDto mappedProfile = modelMapper.map(profile, ProfileDto.class);
        return mappedProfile;
    }
	
	public VersionDTO<ProfileDto> maptoVersionDto(VersionDTO<Object> profile) throws Exception {
		VersionDTO<ProfileDto> dto = new VersionDTO<ProfileDto>();
		ProfileDto profileDto = new ProfileDto();
		if (profile != null) {
			dto.setAuthor(profile.getAuthor());
			dto.setCreatedAt(profile.getCreatedAt());
			dto.setCurrentVersion(profile.isCurrentVersion());
			dto.setVersion(profile.getVersion());
			dto.setUpdatedAt(profile.getUpdatedAt());
			if (profile.getEntity() instanceof Profile) {
				profileDto.setId(((Profile) profile.getEntity()).getId());
				profileDto.setDescription(((Profile) profile.getEntity()).getDescription());
				profileDto.setStatus(((Profile) profile.getEntity()).getStatus());
			}
		dto.setEntity(profileDto);
	}
		return dto;

	}
	
	

}
