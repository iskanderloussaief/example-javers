package com.sifast.mapper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sifast.domain.Post;
import com.sifast.domain.PostComment;
import com.sifast.dto.PostCommentDto;
import com.sifast.dto.VersionDTO;
import com.sifast.repository.PostRepository;

@Component
public class PostCommentMapper {

	@Autowired
	private ConfiguredModelMapper modelMapper;
	@Autowired
	PostRepository postRepository;

	public PostComment mapCreateProject(PostCommentDto postComment) throws Exception {
		PostComment mappedProject = modelMapper.map(postComment, PostComment.class);
		Optional<Post> post = postRepository.findById(postComment.getPostId());
		if (!post.isPresent()) {
			throw new Exception();
		}
		mappedProject.setPost(post.get());
		return mappedProject;
	}


	public PostCommentDto maptoDto(PostComment postComment) throws Exception {
		PostCommentDto dto = new PostCommentDto();
		if (postComment != null) {
			dto.setPostId(postComment.getPost().getId());
			dto.setReview(postComment.getReview());
		} else {
			throw new Exception();
		}

		return dto;
	}

	public VersionDTO<PostCommentDto> maptoVersionDto(VersionDTO<Object> postComment) throws Exception {
		VersionDTO<PostCommentDto> dto = new VersionDTO<PostCommentDto>();
		PostCommentDto postDto = new PostCommentDto();
		int postId = 0;

		if (postComment != null) {
			dto.setAuthor(postComment.getAuthor());
			dto.setCreatedAt(postComment.getCreatedAt());
			dto.setCurrentVersion(postComment.isCurrentVersion());
			dto.setVersion(postComment.getVersion());
			dto.setUpdatedAt(postComment.getUpdatedAt());
			if (postComment.getEntity() instanceof PostComment) {
				postDto.setReview(((PostComment) postComment.getEntity()).getReview());
				if (((PostComment) postComment.getEntity()).getPost() != null) {
					postId = ((PostComment) postComment.getEntity()).getPost().getId();
					postDto.setPostId(postId);

				}
			} else {
				((Post) postComment.getEntity()).getComments().forEach(comment -> {
					postDto.setReview(comment.getReview());
					if (comment.getPost() != null) {
						postDto.setPostId(comment.getPost().getId());
					}
				});
			}
			dto.setEntity(postDto);
		} else {
			throw new Exception();
		}

		return dto;
	}

}
