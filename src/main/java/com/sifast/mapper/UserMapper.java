package com.sifast.mapper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sifast.domain.Profile;
import com.sifast.domain.User;
import com.sifast.dto.UserDto;
import com.sifast.dto.VersionDTO;
import com.sifast.repository.ProfileRepository;

@Component
public class UserMapper {

	@Autowired
	private ConfiguredModelMapper modelMapper;

	@Autowired
	private ProfileRepository profileRepository;

	public User mapUserDtoToUser(UserDto userDto) {
		User mappedUser = modelMapper.map(userDto, User.class);
		Optional<Profile> profile = profileRepository.findById(userDto.getProfile());
		if (profile.isPresent()) {
			mappedUser.setProfile(profile.get());

		} else {
			try {
				throw new Exception();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mappedUser;
	}

	public UserDto mapUserDtoToUser(User user) {
		UserDto mappedUser = new UserDto();
		mappedUser.setUsername(user.getUsername());
		mappedUser.setEmail(user.getEmail());
		mappedUser.setPassword(user.getPassword());
		mappedUser.setId(user.getId());
		if (user.getProfile() != null) {
			mappedUser.setProfile(user.getProfile().getId());
		}
		return mappedUser;
	}

	public VersionDTO<UserDto> maptoVersionDto(VersionDTO<Object> user) throws Exception {
		VersionDTO<UserDto> dto = new VersionDTO<UserDto>();
		UserDto userDto = new UserDto();
		if (user != null) {
			dto.setAuthor(user.getAuthor());
			dto.setCreatedAt(user.getCreatedAt());
			dto.setCurrentVersion(user.isCurrentVersion());
			dto.setVersion(user.getVersion());
			dto.setUpdatedAt(user.getUpdatedAt());
			if (user.getEntity() instanceof User) {
				userDto.setId(((User) user.getEntity()).getId());
				userDto.setUsername(((User) user.getEntity()).getUsername());
				userDto.setEmail(((User) user.getEntity()).getEmail());
				userDto.setPassword(((User) user.getEntity()).getPassword());
				if (((User) user.getEntity()).getProfile() != null) {
					userDto.setProfile(((User) user.getEntity()).getProfile().getId());
				}
			}
			dto.setEntity(userDto);
		}
		return dto;

	}

}
