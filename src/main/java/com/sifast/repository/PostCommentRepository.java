package com.sifast.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sifast.domain.PostComment;

@Repository
public interface PostCommentRepository extends JpaRepository<PostComment, Integer> {

}
